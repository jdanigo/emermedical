module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1341),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', 'd1415ba2aecbdd5d62878cef1ffafec3'),
    },
  },
});
