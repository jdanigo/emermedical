module.exports = {
  jwtSecret: process.env.JWT_SECRET || 'c4a9e785-498c-4172-aa0b-011431635d39',
  jwt: {
    expiresIn: "1d",
  }
};