import React from 'react';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom";
function Navbar(props){
    const history = useHistory();
    const [isNavCollapsed, setIsNavCollapsed] = React.useState(false);
    const location = history.location.pathname;
    return(
        <>
        <div className="header-container fixed-top">
        <header className={` header navbar navbar-expand-sm ${isNavCollapsed ? 'expand-header' : ''} `}>
                    <ul className="navbar-nav theme-brand flex-row  text-center">
                        <li className="nav-item theme-logo">
                            <a href="/dashboard">
                                {/* <img src="../assets/img/logo.png" style={{width:120, height:70}} className="navbar-logo" alt="logo" /> */}
                                
                            </a>
                        </li>
                        <li className="nav-item theme-text">
                            <a href="#" className="nav-link">  </a>
                        </li>
                        <li className="nav-item toggle-sidebar">
                        <a href="#" onClick={()=> {setIsNavCollapsed(!isNavCollapsed)}} className="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-list"><line x1={8} y1={6} x2={21} y2={6} /><line x1={8} y1={12} x2={21} y2={12} /><line x1={8} y1={18} x2={21} y2={18} /><line x1={3} y1={6} x2={3} y2={6} /><line x1={3} y1={12} x2={3} y2={12} /><line x1={3} y1={18} x2={3} y2={18} /></svg></a>
                        </li>
                    </ul>
                    <ul className="navbar-item flex-row navbar-dropdown ml-auto">
                        <li className="nav-item dropdown user-profile-dropdown  order-lg-0 order-1">
                            <a href="javascript:void(0);" className="nav-link dropdown-toggle user" id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-settings"><circle cx={12} cy={12} r={3} /><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z" /></svg>
                            </a>
                            <div className="dropdown-menu position-absolute animated fadeInUp" aria-labelledby="userProfileDropdown">
                                <div className="user-profile-section">
                                    <div className="media mx-auto">
                                        <img src="../assets/img/90x90.jpg" className="img-fluid mr-2" alt="avatar" />
                                        <div className="media-body">
                                            <h5>Daniel Garcés</h5>
                                            <p>Administrador</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="dropdown-item">
                                    <a href="user_profile.html">
                                        <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" /><circle cx={12} cy={7} r={4} /></svg> <span>My Profile</span>
                                    </a>
                                </div>
                                <div className="dropdown-item">
                                    <a href="apps_mailbox.html">
                                        <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-inbox"><polyline points="22 12 16 12 14 15 10 15 8 12 2 12" /><path d="M5.45 5.11L2 12v6a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2v-6l-3.45-6.89A2 2 0 0 0 16.76 4H7.24a2 2 0 0 0-1.79 1.11z" /></svg> <span>My Inbox</span>
                                    </a>
                                </div>
                                <div className="dropdown-item">
                                    <a href="auth_lockscreen.html">
                                        <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-lock"><rect x={3} y={11} width={18} height={11} rx={2} ry={2} /><path d="M7 11V7a5 5 0 0 1 10 0v4" /></svg> <span>Lock Screen</span>
                                    </a>
                                </div>
                                <div className="dropdown-item">
                                    <a href="auth_login.html">
                                        <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-log-out"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4" /><polyline points="16 17 21 12 16 7" /><line x1={21} y1={12} x2={9} y2={12} /></svg> <span>Log Out</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </header>
            </div>
        </>
    );

}

export default Navbar;