import { environment } from './environment';
export default {
    url: environment.production ? 'https://apis.cloudsoft.site/emermedical/' : 'http://localhost:1341/',
    urlqrcode: environment.production ? 'https://hc.sosemermedical.com/dpx/' : 'http://localhost:3000/dpx/',
}