import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import { useQuery, useMutation } from "@apollo/react-hooks";
import axios from "axios";
import Config from "../../Config";
import { withFormik } from 'formik';
import {ToastContainer, toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const CREAR_HISTORIA_CLINICA = gql`
mutation crearHistoriaClinica($data: HistoriaClinicaInput){
  
  createHistoriaClinica(input: { data: $data }){
    historiaClinica{
      hora_llamada
      fecha
      hora_llegada
      zona
      no_movil
      placa_movil
      lugar_origen_ocurrencia
      motivo_descripcion_traslado
      tipo_servicio
      ciudad
      departamento
      servicio_prestado
      condicion_accidentado
      tipo_vehiculo
      placa
      marca
      aseguradora
      poliza
      at
      no_poliza
      escala_glassgow
      frecuencia_respiratoria
      frecuencia_cardiaca
      saturacion
      temperatura
      glucometria
      oxigeno
      presion_arterial_diastolica
      presion_arterial_sistolica
      analisis_observaciones_traslado
      nombre_institucion_traslado_final
      servicio_traslado_final
      habilitacion_cama_traslado_final
      ciudad_traslado_final
      departamento_traslado_final
      hora_traslado_final
      fecha_traslado_final
      autoriza_traslado
      firma_medico
      registro_medico
      firma_paciente
      documento_paciente
      tripulante_ambulancia
      documento_tripulante_ambulancia
      conductor_ambulancia
      documento_conductor_ambulancia
      paciente
    }
  }
}
`;

const CREAR_PACIENTE = gql`
mutation crearPaciente($dataPaciente : PacienteInput){
  createPaciente(input: { data: $dataPaciente}){
    paciente{
      nombre_completo
      tipo_documento
      numero_documento
      fecha_nacimiento
      edad
      sexo
      eps
      estado_civil
      direccion
      ocupacion
      telefono
      ciudad
      departamento
      nombre_acompa
      parentesco
      diagnostico_enfermedades
      antecedentespatologicos_alergias
      antecedentespatologicos_familiares
      antecedentespatologicos_cirugias
      antecedentespatologicos_medicamentos
    }
  }
}
`; 

const GET_PACIENTES = gql`
  query BuscarPaciente($numero: String) {
    pacientes(where: { numero_documento: $numero }) {
      _id
      nombre_completo
      tipo_documento
      numero_documento
      fecha_nacimiento
      edad
      sexo
      eps
      estado_civil
      direccion
      ocupacion
      telefono
      ciudad
      departamento
      nombre_acompa
      parentesco
      diagnostico_enfermedades
      antecedentespatologicos_alergias
      antecedentespatologicos_familiares
      antecedentespatologicos_cirugias
      antecedentespatologicos_medicamentos
    }
  }
`;

const GET_ALL_PACIENTES = gql`
  query {
    pacientes {
      _id
      nombre_completo
      tipo_documento
      numero_documento
      fecha_nacimiento
      edad
      sexo
      eps
      estado_civil
      direccion
      ocupacion
      telefono
      ciudad
      departamento
      nombre_acompa
      parentesco
      diagnostico_enfermedades
      antecedentespatologicos_alergias
      antecedentespatologicos_familiares
      antecedentespatologicos_cirugias
      antecedentespatologicos_medicamentos
    }
  }
`;

const NuevoPaciente = (props) => {

  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
    setValues
  } = props;

  const [ tipo_doc, setTipoDocumento] = React.useState(null);
  const [ documento, setDocumento] = React.useState(null);
  const [showFormPaciente, setShowFormPaciente] = React.useState(false);
  const [PacienteExiste, setPacienteExiste] = React.useState(true);
  const [showFormHc, setShowFormHc] = React.useState(false);
  const [buttonLoading, setButtonLoading] = React.useState(false);
  const [idPaciente, setIdPaciente] = React.useState(null);

  const {data: dataR,error: errorR,loading: loadingR, refetch: refetChing,} = useQuery(GET_PACIENTES);
  //console.log(dataR, "mostrando el resultado de un solo paciente");
  
  const [addPaciente] = useMutation(CREAR_PACIENTE);
  const [addHc] = useMutation(CREAR_HISTORIA_CLINICA);

  const BuscarPaciente = () => {
    //values.nombre_completo = 'Jose';
    setValues(values);
    refetChing({
      numero: documento,
      tipo_documento: tipo_doc
    })
    .then(response => {
      console.log(response);
      if(response.data.pacientes.length >0){
        values.id_paciente = response.data.pacientes[0]._id;
        setIdPaciente(response.data.pacientes[0]._id);
        setShowFormPaciente(true);
        setShowFormHc(true);
      }else{
        toast.error("Paciente no encontrado");
        setShowFormPaciente(true);
        setPacienteExiste(false);
        setShowFormHc(false);

        

      }
    })
    .catch(error => {
      console.log(error);
    })
  };

  const crearPaciente = () => (
  setButtonLoading(true),
    addPaciente({
      variables: { dataPaciente: values },
    }).then(response => {
      toast.success("Paciente creado exitosamente");
      console.log(response);
      setPacienteExiste(true);
      setButtonLoading(false);
      setDocumento(response.data.createPaciente.paciente.numero_documento);
      BuscarPaciente();
      setShowFormHc(true);
    })
    .catch(error => {
      console.log(error);
    })
  )

  return (
    <>
    <ToastContainer />
      <form onSubmit={handleSubmit}>
        {/* INICIO DEL FORMULARIO */}

        {/* DATOS DEL PACIENTE */}
        <div className="row layout-top-spacing">
          <div className="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div className="statbox widget box box-shadow">
              <div className="widget-header">
                <div className="row">
                  <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>DATOS DEL PACIENTE</h4>
                  </div>
                </div>
              </div>
              <div className="widget-content widget-content-area">
                <div className="row">
                  <div className="col-12">
                    <div className="input-group mb-4">
                      <div className="input-group-prepend">
                        <select
                          className="form-control"
                          onChange={e => {handleChange(e);}}
                          onBlur={handleBlur}
                          name="tipo_documento"
                          value={values.tipo_documento}
                        >
                          <option>Tipo Documento</option>
                          <option value="CC">Cédula de Ciudadanía</option>
                        </select>
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        onChange={e => {
                          handleChange(e);
                          setDocumento(e.target.value);
                        }}
                        onBlur={handleBlur}
                        name="numero_documento"
                        value={values.numero_documento}
                      />
                      {errors.numero_documento && touched.numero_documento && errors.numero_documento}
                      <div className="input-group-append">
                        <button
                          type="button"
                          onClick={BuscarPaciente}
                          className="btn btn-outline-info"
                        >
                          Buscar
                              </button>
                      </div>
                    </div>
                  </div>

                  {showFormPaciente &&
                  <>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Nombre Completo</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="nombre_completo"
                        value={values.nombre_completo || ''}
                      />
                      {errors.nombre_completo && touched.nombre_completo && errors.nombre_completo}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Fecha de Nacimiento</label>
                      <input
                        type="date"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="fecha_nacimiento"
                        value={values.fecha_nacimiento || ''}
                      />
                      {errors.fecha_nacimiento && touched.fecha_nacimiento && errors.fecha_nacimiento}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Edad</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="edad"
                        value={values.edad || ''}
                      />
                      {errors.edad && touched.edad && errors.edad}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Sexo</label>
                      <select
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="sexo"
                        value={values.sexo || ''}
                      >
                        <option>Seleccionar Opción</option>
                        <option value="Masculino">Masculino</option>
                        <option value="Femenino">Femenino</option>
                        <option value="Sin Definir">Sin Definir</option>
                      </select>
                      {errors.sexo && touched.sexo && errors.sexo}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>EPS</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="eps"
                        value={values.eps || ''}
                      />
                      {errors.eps && touched.eps && errors.eps}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Estado Civil</label>
                      <select
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="estado_civil"
                        value={values.estado_civil || ''}
                      >
                        <option>Seleccionar Opción</option>
                        <option value="Soltero">Soltero</option>
                        <option value="Casado">Casado</option>
                        <option value="Union Libre">Union Libre</option>
                        <option value="Viudo">Viudo</option>
                      </select>
                      {errors.estado_civil && touched.estado_civil && errors.estado_civil}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Dirección</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="direccion"
                        value={values.direccion || ''}
                      />
                      {errors.direccion && touched.direccion && errors.direccion}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Ocupación</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="ocupacion"
                        value={values.ocupacion || ''}
                      />
                      {errors.ocupacion && touched.ocupacion && errors.ocupacion}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Teléfono</label>
                      <input
                        type="number"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="telefono"
                        value={values.telefono || ''}
                      />
                      {errors.telefono && touched.telefono && errors.telefono}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Ciudad</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="ciudad"
                        value={values.ciudad || ''}
                      />
                      {errors.ciudad && touched.ciudad && errors.ciudad}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Departamento</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="departamento"
                        value={values.departamento || ''}
                      />
                      {errors.departamento && touched.departamento && errors.departamento}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Nombre Acompañante</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="nombre_acompa"
                        value={values.nombre_acompa || ''}
                      />
                      {errors.nombre_acompa && touched.nombre_acompa && errors.nombre_acompa}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Parentezco</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="parentesco"
                        value={values.parentesco || ''}
                      />
                      {errors.parentesco && touched.parentesco && errors.parentesco}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Diagnóstico de Enfermedades</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="diagnostico_enfermedades"
                        value={values.diagnostico_enfermedades || ''}
                      />
                      {errors.diagnostico_enfermedades && touched.diagnostico_enfermedades && errors.diagnostico_enfermedades}
                    </div>
                  </div>

                  <h3>Antecedentes Patológicos</h3>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Alergias</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="antecedentespatologicos_alergias"
                        value={values.antecedentespatologicos_alergias || ''}
                      />
                      {errors.antecedentespatologicos_alergias && touched.antecedentespatologicos_alergias && errors.antecedentespatologicos_alergias}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Familiares</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="antecedentespatologicos_familiares"
                        value={values.antecedentespatologicos_familiares || ''}
                      />
                      {errors.antecedentespatologicos_familiares && touched.antecedentespatologicos_familiares && errors.antecedentespatologicos_familiares}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Cirugias</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="antecedentespatologicos_cirugias"
                        value={values.antecedentespatologicos_cirugias || ''}
                      />
                      {errors.antecedentespatologicos_cirugias && touched.antecedentespatologicos_cirugias && errors.antecedentespatologicos_cirugias}
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Medicamentos</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="antecedentespatologicos_medicamentos"
                        value={values.antecedentespatologicos_medicamentos || ''}
                      />
                      {errors.antecedentespatologicos_medicamentos && touched.antecedentespatologicos_medicamentos && errors.antecedentespatologicos_medicamentos}
                    </div>
                  </div>


                  </>

                  }

                  
                </div>
                {!PacienteExiste &&
                <button type="button" onClick={crearPaciente} className="btn btn-success" disabled={buttonLoading}>
                Crear Paciente
                </button>
                }
                
              </div>
              
            </div>
          </div>
          
        </div>
        {showFormHc &&
        
        <>
        {/* DATOS DEL TRASLADO */}
        <div className="row layout-top-spacing">
          <div className="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div className="statbox widget box box-shadow">
              <div className="widget-header">
                <div className="row">
                  <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>DATOS DEL TRASLADO</h4>
                  </div>
                </div>
              </div>
              <div className="widget-content widget-content-area">
                <div className="row">
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Ciudad</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="ciudad"
                        value={values.ciudad || ''}
                      />
                      {errors.ciudad && touched.ciudad && errors.ciudad}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Departamento</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="departamento"
                        value={values.departamento || ''}
                      />
                      {errors.departamento &&
                        touched.departamento &&
                        errors.departamento}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Hora Llamado</label>
                      <input
                        type="time"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="hora_llamada"
                        value={values.hora_llamada || ''}
                      />
                      {errors.hora_llamada &&
                        touched.hora_llamada &&
                        errors.hora_llamada}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Fecha</label>
                      <input
                        type="date"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="fecha"
                        value={values.fecha || ''}
                      />
                      {errors.fecha && touched.fecha && errors.fecha}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Hora Llegada</label>
                      <input
                        type="time"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="hora_llegada"
                        value={values.hora_llegada || ''}
                      />
                      {errors.hora_llegada &&
                        touched.hora_llegada &&
                        errors.hora_llegada}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Zona</label>
                      <select
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="zona"
                        value={values.zona || ''}
                      >
                        <option>Seleccione una opción</option>
                        <option>Urbana</option>
                        <option>Rural</option>
                      </select>
                      {errors.zona && touched.zona && errors.zona}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>No Móvil</label>
                      <select
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="no_movil"
                        value={values.no_movil || ''}
                      >
                        <option>Seleccione una opción</option>
                        <option>121</option>
                        <option>43</option>
                      </select>
                      {errors.no_movil &&
                        touched.no_movil &&
                        errors.no_movil}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Placa Móvil</label>
                      <select
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="placa_movil"
                        value={values.placa_movil || ''}
                      >
                        <option>Seleccione una opción</option>
                        <option value="DCO 446">DCO 446</option>
                        <option value="RIY 482">RIY 482</option>
                      </select>
                      {errors.placa_movil &&
                        touched.placa_movil &&
                        errors.placa_movil}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Lugar Origen Y/O Ocurrencia</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="lugar_origen_ocurrencia"
                        value={values.lugar_origen_ocurrencia || ''}
                      />
                      {errors.lugar_origen_ocurrencia &&
                        touched.lugar_origen_ocurrencia &&
                        errors.lugar_origen_ocurrencia}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Motivo y Descripción del traslado</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="motivo_descripcion_traslado"
                        value={values.motivo_descripcion_traslado || ''}
                      />
                      {errors.motivo_descripcion_traslado &&
                        touched.motivo_descripcion_traslado &&
                        errors.motivo_descripcion_traslado}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Tipo de Servicio</label>
                      <select
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="tipo_servicio"
                        value={values.tipo_servicio || ''}
                      >
                        <option>Seleccionar una opción</option>
                        <option value="TAB">
                          TAB (Traslado Asistencial Basico)
                              </option>
                        <option value="TAM">
                          TAM (Traslado Asistencial Medicalizado)
                              </option>
                        <option value="APH">
                          APH (Atención Prehospitalaria)
                              </option>
                        <option value="RDO">RDO (Redondo)</option>
                        <option value="Sencillo">Sencillo</option>
                      </select>
                      {errors.tipo_servicio &&
                        touched.tipo_servicio &&
                        errors.tipo_servicio}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Servicio Prestado</label>
                      <select
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="servicio_prestado"
                        value={values.servicio_prestado || ''}
                      >
                        <option>Seleccionar una opción</option>
                        <option value="Enfermedad General">
                          Enfermedad General
                              </option>
                        <option value="Accidente de Tránsito">
                          Accidente de Tránsito
                              </option>
                        <option value="Accidente Laboral">
                          Accidente Laboral
                              </option>
                        <option value="Accidente Estudiantil">
                          Accidente Estudiantil
                              </option>
                        <option value="Servicio Social">
                          Servicio Social
                              </option>
                        <option value="Otro">Otro</option>
                      </select>
                      {errors.servicio_prestado &&
                        touched.servicio_prestado &&
                        errors.servicio_prestado}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* DATOS DEL ACCIDENTE DE TRÁNSITO */}
        <div className="row layout-top-spacing">
          <div className="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div className="statbox widget box box-shadow">
              <div className="widget-header">
                <div className="row">
                  <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>DATOS DEL ACCIDENTE DE TRÁNSITO</h4>
                  </div>
                </div>
              </div>
              <div className="widget-content widget-content-area">
                <div className="row">
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Condición del accidentado</label>
                      <select
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="condicion_accidentado"
                        value={values.condicion_accidentado || ''}
                      >
                        <option>Seleccionar una opción</option>
                        <option value="Conductor">Conductor</option>
                        <option value="Ocupante">Ocupante</option>
                        <option value="Peaton">Peaton</option>
                        <option value="Ciclista">Ciclista</option>
                        <option value="Otro">Otro</option>
                      </select>
                      {errors.condicion_accidentado &&
                        touched.condicion_accidentado &&
                        errors.condicion_accidentado}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Tipo de Vehículo</label>
                      <select
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="tipo_vehiculo"
                        value={values.tipo_vehiculo || ''}
                      >
                        <option>Seleccionar una opción</option>
                        <option value="Motocicleta">Motocicleta</option>
                        <option value="Automovil">Automovil</option>
                        <option value="Camioneta">Camioneta</option>
                        <option value="Bus">Bus</option>
                        <option value="Micro Bus">Micro Bus</option>
                        <option value="Vehículo Pesado">
                          Vehículo Pesado
                              </option>
                        <option value="Otro">Otro</option>
                      </select>
                      {errors.tipo_vehiculo &&
                        touched.tipo_vehiculo &&
                        errors.tipo_vehiculo}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Placa</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="placa"
                        value={values.placa || ''}
                      />
                      {errors.placa && touched.placa && errors.placa}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Marca</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="marca"
                        value={values.marca || ''}
                      />
                      {errors.marca && touched.marca && errors.marca}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Aseguradora</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="aseguradora"
                        value={values.aseguradora || ''}
                      />
                      {errors.aseguradora &&
                        touched.aseguradora &&
                        errors.aseguradora}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Póliza</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="poliza"
                        value={values.poliza || ''}
                      />
                      {errors.poliza && touched.poliza && errors.poliza}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>AT</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="at"
                        value={values.at || ''}
                      />
                      {errors.at && touched.at && errors.at}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>No</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="no_poliza"
                        value={values.no_poliza || ''}
                      />
                      {errors.no_poliza &&
                        touched.no_poliza &&
                        errors.no_poliza}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* EXAMEN FÍSICO EN INICIO DEL TRASLADO */}
        <div className="row layout-top-spacing">
          <div className="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div className="statbox widget box box-shadow">
              <div className="widget-header">
                <div className="row">
                  <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>EXAMEN FÍSICO EN INICIO DEL TRASLADO</h4>
                  </div>
                </div>
              </div>
              <div className="widget-content widget-content-area">
                <div className="row">
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Escala de Glassgow</label>
                      <select
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="escala_glassgow"
                        value={values.escala_glassgow || ''}
                      >
                        <option>Seleccionar opción</option>
                        <option value="1">1/15</option>
                        <option value="2">2/15</option>
                        <option value="3">3/15</option>
                        <option value="4">4/15</option>
                        <option value="5">5/15</option>
                        <option value="6">6/15</option>
                        <option value="7">7/15</option>
                        <option value="8">8/15</option>
                        <option value="9">9/15</option>
                        <option value="10">10/15</option>
                        <option value="11">11/15</option>
                        <option value="12">12/15</option>
                        <option value="13">13/15</option>
                        <option value="14">14/15</option>
                        <option value="15">15/15</option>
                      </select>
                      {errors.escala_glassgow &&
                        touched.escala_glassgow &&
                        errors.escala_glassgow}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Frecuencia Respiratoria /MIN</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="frecuencia_respiratoria"
                        value={values.frecuencia_respiratoria || ''}
                      />
                      {errors.frecuencia_respiratoria &&
                        touched.frecuencia_respiratoria &&
                        errors.frecuencia_respiratoria}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Frecuencia Cardiaca /MIN</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="frecuencia_cardiaca"
                        value={values.frecuencia_cardiaca || ''}
                      />
                      {errors.frecuencia_cardiaca &&
                        touched.frecuencia_cardiaca &&
                        errors.frecuencia_cardiaca}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Saturación de Oxígeno /SPO2</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="saturacion"
                        value={values.saturacion || ''}
                      />
                      {errors.saturacion &&
                        touched.saturacion &&
                        errors.saturacion}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Temperatura /Grados</label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="temperatura"
                        value={values.temperatura || ''}
                      />
                      {errors.temperatura &&
                        touched.temperatura &&
                        errors.temperatura}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Glucometría </label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="glucometria"
                        value={values.glucometria || ''}
                      />
                      {errors.glucometria &&
                        touched.glucometria &&
                        errors.glucometria}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Oxígeno </label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="oxigeno"
                        value={values.oxigeno || ''}
                      />
                      {errors.oxigeno && touched.oxigeno && errors.oxigeno}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Presión arterial sistólica </label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="presion_arterial_sistolica"
                        value={values.presion_arterial_sistolica || ''}
                      />
                      {errors.presion_arterial_sistolica &&
                        touched.presion_arterial_sistolica &&
                        errors.presion_arterial_sistolica}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <label>Presión arterial diastólica </label>
                      <input
                        type="text"
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="presion_arterial_diastolica"
                        value={values.presion_arterial_diastolica || ''}
                      />
                      {errors.presion_arterial_diastolica &&
                        touched.presion_arterial_diastolica &&
                        errors.presion_arterial_diastolica}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* ANÁLISIS Y OBSERVACIONES DURANTE EL TRASLADO */}
        <div className="row layout-top-spacing">
          <div className="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div className="statbox widget box box-shadow">
              <div className="widget-header">
                <div className="row">
                  <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>ANÁLISIS Y OBSERVACIONES DURANTE EL TRASLADO</h4>
                  </div>
                </div>
              </div>
              <div className="widget-content widget-content-area">
                <div className="row">
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <textarea
                        className="form-control"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="analisis_observaciones_traslado"
                        defaultValue={values.analisis_observaciones_traslado || ''}
                      >

                      </textarea>
                      {errors.analisis_observaciones_traslado &&
                        touched.analisis_observaciones_traslado &&
                        errors.analisis_observaciones_traslado}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* TRASLADO FINAL  IPS/HOSPITAL/CENTRO MÉDICO/ */}
        <div className="row layout-top-spacing">
          <div className="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div className="statbox widget box box-shadow">
              <div className="widget-header">
                <div className="row">
                  <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>TRASLADO FINAL IPS/HOSPITAL/CENTRO MÉDICO/</h4>
                  </div>
                </div>
              </div>
              <div className="widget-content widget-content-area">
                <div className="row">
                  <div className="col-12">
                    <div className="form-group mb-3">
                      <textarea className="form-control"></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* FIN DEL FORMULARIO */}
        {showFormHc &&
          <button type="submit" className="btn btn-success" disabled={isSubmitting}>
            Crear Historia Clínica
          </button>
        }
        


        </>
        
        }

        

        

        

        

        

        
      </form>
    </>
  );
}

export default withFormik(
  {
    enableReinitialize: true,
    // default values
    mapPropsToValues: () => ({
      numero_documento: ''
    }),
    validate: values => {
      // validation
    },
    handleSubmit: (values, { setSubmitting }) => {

      //console.log(PacienteExiste);

      setTimeout(() => {

        alert(JSON.stringify(values, null, 2));

        setSubmitting(false);

      }, 1000);

    },
  }
)(NuevoPaciente);
